This week's commits again all went into the core python plugin repository:
https://gitlab.com/qgisapi/networkapi/commits/master


1. What did you get done this week?

- refactored HTTP request processing to support incremental parsing of
POST request bodies -- this part of the code is overall more robust
now

- implemented adding of layers from both external URIs and POST data

- implemented layer information retrieval, making use of automatic
conversion of QGIS-specific types using a custom JSONEncoder

- added some documentation to the gitlab repo README so that
interested QGIS users know how to play around with/test the
development versions of the plugin (will send out dedicated email to
the QGIS-dev list this afternoon!)


2. What do you plan on doing next week?

- finalise request structure and refactor request path lookup

- implement more request paths and necessary conversion functions
(particularly for geodata readout)

- start assembling documentation of request paths

- get started on R package


3. Are you blocked on anything?

- this week's mentor meeting had to be cancelled last minute, still
waiting for request structure to be finalised next week so I can get
started on documentation.
