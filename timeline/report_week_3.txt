This week's commits are split between the QGIS plugin:
https://gitlab.com/qgisapi/networkapi/commits/master
and the R package:
https://gitlab.com/qgisapi/rqgisapi/commits/master

1. What did you get done this week?

- big refactor of the function registration/dispatch using decorators

- it was decided that documentation of the web API would be provided
*through the API itself* by serving the python functions' docstrings,
so started work on that

- created R package skeleton and implemented a generic query using the
request package

2. What do you plan on doing next week?

In anticipation of the first evaluation period, focus is on wrapping
up work on currently incomplete features:

- make plugin options configurable via Qt Dialog

- document current set of wrapped functions and implement delivery of
docs through the web API

- Implement current function set in R client package (including sp
object conversion)

3. Are you blocked on anything?

No