# Log of Meetings

## 09 May 2017 : appear.in

### Present: BR, KS, JW

Initial meet and greet meeting. 

### Actions:

 * BR to set up repository
 * KS to join qgis-devel and say "Hi"

## 19 June 2017 : appear.in

### Present: BR, KS

Discussion of project progress so far (coding weeks 1-3) -- work spread between many fronts (Qt GUI, HTTP server, wrapping QGIS functions, R package) but progress on all

Specific questions discussed:

* What exactly needs to be handed in for the 1st evaluation next week? Just a report? Code as well?
    * Evaluation is actually just a readymade form to be submitted to Google, shouldn't take long. Weekly reporting and outreach to the OSGeo-lists should continue as usual.
* Are the plugin options shown in the mock config dialog reasonable?
    * Remove 'automatically listen' option -- replace with a permanent widget/button in the QGIS toolbar to toggle on/off
* Is the hard-coded html around the API docs ok or is there a neater
way to do this?
    * Current approach is probably sufficient, also good to keep dependencies low. While creating docstrings for functions, adhere to [PEP 257 conventions](https://www.python.org/dev/peps/pep-0257/)
* When to start on working out use cases?
    * As soon as plugin config is working, possibly next week? Barry might be unavailable then, so could use next week's meeting to focus on discussing use case/notebook ideas with Julia

### Actions:

* Continue work:
    * Focus on wrapping up work on Qt/UI components (especially plugin configuration)
    * Wrap QGIS functions for QgsFeature readout (+necessary conversions)
    * For geodata wrapping of R package, use classes from the new [`sf`](https://CRAN.R-project.org/package=sf) rather than the legacy `sp` package
* KS to write up meeting minutes

## 29 June 2017 : appear.in

### Present: BR, JW, KS

Discussion of project progress coding weeks 4+5, primarily Qt GUI and R package.

### Actions based on specific questions discussed:

- the icon-based status bar toggle button can be hard to make out, turn it into a checkbox with descriptive label (curret plugin status can be displayed as mouse-over tooltip)

- in R package, use raster's 'stack' object for image plotting

- keep using settings dependency inside qgis_settings() in order to not clog up R's option() namespace

and the final two core feature tasks before moving on to focus on use cases:

- hook up various buttons in the plugin config dialog to do various tasks (restarting the server on core option changes in particular)

- add QTimer to server to be able to interrupt and clean out dead connections (as well as read out stalled data)

once those is done, move on to cleaning up and re-implementing notebooks from https://github.com/JuliaWagemann/MalariaRisk/

KS is going on a wee holiday next week so not much progress likely, next video call scheduled for July 11th, and hopefully a regular weekly call routine from then on

## 11 July 2017 : appear.in

### Present: BR, JW, KS

- Due to KS being on holiday, the only updates since the last meeting are a rudimentary tutorial for the `rqgisapi` package (http://qgisapi.gitlab.io/rqgisapi/articles/tutorial.html) as well as general documentation update.

### Ideas/discussion points

- BR suggested implementing some Network API methods which block until the user has performed some action within QGIS and signals for the plugin to send an appropriate result (e.g. some features have been selected) back -- might be tricky with current architecture

- BR has been playing with easily transferring (xml) styles between layers

### Actions for the coming week

- Finalise QGIS plugin architecture by adding connection timeout as well as better in-app documentation for the user, advertise package on the R-sig-geo list

- Look at how more sophisticated extraction of geodata is possible using the different QgsFeatureRequest methods

- Get started on converting the MalariaRisk use cases

## 18 July 2017 : appear.in

### Present: JW, KS

Final core plugin features were implemented earlier this week (and announced on the R-sig-geo mailing list from which KS received some responses), so we mostly discussed use cases. KS has started reproducing the existing MalariaRisk sample notebooks in both Python and R, so far they only use two QGIS-specific algorithms/tools, namely the QgsRasterCalculator as well as one processing algorithm.

### Actions for the coming week

JW will try to identify some more QGIS-specific functions not provided by other packages (such as rgdal or rgeos) which would be of interest for use cases, and communicate those to KS.

KS to have a look at providing a general API access point for processing algorithms, as well as adding some more documentation to the wiki.

## 01 August 2017 : appear.in

### Present: BR, JW, KS

Both JW and BR did some playing around with the R package and ran into various problems/bugs when adding layers (related to both WMS provider strings and lack of ability to specify CRSs)

### Actions for the coming week

KS to implement various CRS-related methods and improve the iface.addVector/RasterLayer methods to allow for more easy WMS specification in particular

## 08 August 2017 : appear.in

### Present: BR, KS

Good progress in the past week with a flurry of commits rounding off some core features, as well as the completion of the MalariaRisk notebook.

Discussed a potential powerful way to apply low-level style to different layer types through a JSON-based "QGISPythonClassName": [ constructor-args ] format to be parsed and instantiated on the plugin side.

### Actions for the coming week

Apart from fixes to existing methods, only add the new styling functions but otherwise focus on documentation of the package as well as overall project
